package edu.uchicago.cs.java.finalproject.controller;

import sun.audio.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.sound.sampled.Clip;

import edu.uchicago.cs.java.finalproject.game.model.*;
import edu.uchicago.cs.java.finalproject.game.view.*;
import edu.uchicago.cs.java.finalproject.sounds.Sound;

// ===============================================
// == This Game class is the CONTROLLER
// ===============================================

public class Game implements Runnable, KeyListener {

	// ===============================================
	// FIELDS
	// ===============================================

	public static final Dimension DIM = new Dimension(1100, 900); //the dimension of the game.
	private GamePanel gmpPanel;
	public static Random R = new Random();
	public final static int ANI_DELAY = 45; // milliseconds between screen
											// updates (animation)
	private Thread thrAnim;
	private int nLevel = 1;
	private static int nTick = 0;
	private ArrayList<Tuple> tupMarkForRemovals;
	private ArrayList<Tuple> tupMarkForAdds;
	private boolean bMuted = false;
    private int totalRows = 5;

    private static final int NUM_ALIENS = 1;
	

	private final int PAUSE = 80, // p key
			QUIT = 81, // q key
			LEFT = 37, // rotate left; left arrow
			RIGHT = 39, // rotate right; right arrow
			UP = 38, // thrust; up arrow
			START = 83, // s key
			FIRE = 32, // space key
			MUTE = 77, // m-key mute

	// for possible future use
	// HYPER = 68, 					// d key
	// SHIELD = 65, 				// a key arrow
	// NUM_ENTER = 10, 				// hyp
	 SPECIAL = 70; 					// fire special weapon;  F key

	private Clip clpThrust;
	private Clip clpMusicBackground;

	private static final int SPAWN_NEW_SHIP_FLOATER = 200;



	// ===============================================
	// ==CONSTRUCTOR
	// ===============================================

	public Game() {

		gmpPanel = new GamePanel(DIM);
		gmpPanel.addKeyListener(this);

		clpThrust = Sound.clipForLoopFactory("whitenoise.wav");
		clpMusicBackground = Sound.clipForLoopFactory("space_invaders_intro.wav");
	}

	// ===============================================
	// ==METHODS
	// ===============================================

	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() { // uses the Event dispatch thread from Java 5 (refactored)
					public void run() {
						try {
							Game game = new Game(); // construct itself
							game.fireUpAnimThread();

						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
	}

	private void fireUpAnimThread() { // called initially
		if (thrAnim == null) {
			thrAnim = new Thread(this); // pass the thread a runnable object (this)
			thrAnim.start();
		}
	}

	// implements runnable - must have run method
	public void run() {

		// lower this thread's priority; let the "main" aka 'Event Dispatch'
		// thread do what it needs to do first
		thrAnim.setPriority(Thread.MIN_PRIORITY);

		// and get the current time
		long lStartTime = System.currentTimeMillis();

		// this thread animates the scene
		while (Thread.currentThread() == thrAnim) {
			tick();
			spawnNewShipFloater();
			gmpPanel.update(gmpPanel.getGraphics()); // update takes the graphics context we must 
														// surround the sleep() in a try/catch block
														// this simply controls delay time between 
														// the frames of the animation

			//this might be a good place to check for collisions
			checkCollisions();
			//this might be a god place to check if the level is clear (no more foes)
			//if the level is clear then spawn some big asteroids -- the number of asteroids 
			//should increase with the level.
            if (isLevelClear())
			    checkNewLevel();

            spawnUFO();

			try {
				// The total amount of time is guaranteed to be at least ANI_DELAY long.  If processing (update) 
				// between frames takes longer than ANI_DELAY, then the difference between lStartTime - 
				// System.currentTimeMillis() will be negative, then zero will be the sleep time
				lStartTime += ANI_DELAY;
				Thread.sleep(Math.max(0,
						lStartTime - System.currentTimeMillis()));
			} catch (InterruptedException e) {
				// just skip this frame -- no big deal
				continue;
			}
		} // end while
	} // end run

	private void checkCollisions() {

		//@formatter:off
		//for each friend in movFriends
			//for each foe in movFoes
				//if the distance between the two centers is less than the sum of their radii
					//mark it for removal
		
		//for each mark-for-removal
			//remove it
		//for each mark-for-add
			//add it
		//@formatter:on
		
		//we use this ArrayList to keep pairs of movMovables/movTarget for either
		//removal or insertion into our arrayLists later on
		tupMarkForRemovals = new ArrayList<Tuple>();
		tupMarkForAdds = new ArrayList<Tuple>();

		Point pntFriendCenter, pntFoeCenter;
		int nFriendRadiux, nFoeRadiux;

		for (Movable movFriend : CommandCenter.movFriends) {
			for (Movable movFoe : CommandCenter.movFoes) {

				pntFriendCenter = movFriend.getCenter();
				pntFoeCenter = movFoe.getCenter();
				nFriendRadiux = movFriend.getRadius();
				nFoeRadiux = movFoe.getRadius();

				//detect collision
				if (pntFriendCenter.distance(pntFoeCenter) < (nFriendRadiux + nFoeRadiux)) {
                    // add code for barricade here
                    if (movFriend instanceof Barricade) {
                        Barricade bar = (Barricade) movFriend; // safe cast because we know its a barricade
                        bar.decreaseHealth();
                        if (bar.getHealth() == 0) {
                            tupMarkForRemovals.add(new Tuple(CommandCenter.movFriends, movFriend));
                            killFoe(movFoe);
                        }
                        else {
                            killFoe(movFoe);
                        }

                    }
                    else if ((movFriend instanceof GalagaFalcon)) {
                        if (!CommandCenter.getGalagaFalcon().getProtected()) {
                            Sound.playSound("explosion.wav");
                            tupMarkForRemovals.add(new Tuple(CommandCenter.movFriends, movFriend));
                            CommandCenter.spawnGalagaFalcon(false);
                            killFoe(movFoe);
                        }
                    }
					//not the falcon
					else {
						tupMarkForRemovals.add(new Tuple(CommandCenter.movFriends, movFriend));
						killFoe(movFoe);
                        if (movFriend instanceof GalagaBullet && movFoe instanceof Alien)
                            //CommandCenter.movFloaters.add(new Explosion((GalagaBullet) movFriend,movFoe.getRadius()));
                            drawExplosion((GalagaBullet) movFriend, movFoe.getRadius());
					}//end else

					//explode/remove foe
				}//end if
			}//end inner for
		}//end outer for


		//check for collisions between falcon and floaters
		if (CommandCenter.getGalagaFalcon() != null){
			Point pntFalCenter = CommandCenter.getGalagaFalcon().getCenter();
			int nFalRadiux = CommandCenter.getGalagaFalcon().getRadius();
			Point pntFloaterCenter;
			int nFloaterRadiux;

			for (Movable movFloater : CommandCenter.movFloaters) {
				pntFloaterCenter = movFloater.getCenter();
				nFloaterRadiux = movFloater.getRadius();

				//detect collision
				if (pntFalCenter.distance(pntFloaterCenter) < (nFalRadiux + nFloaterRadiux)) {
                    System.out.println("COLLISION DETECTED");
                    tupMarkForRemovals.add(new Tuple(CommandCenter.movFloaters, movFloater));
					Sound.playSound("pacman_eatghost.wav");
                    CommandCenter.setNumGalagaFalcons(CommandCenter.getNumGalagaFalcons() + 1);
				}//end if
			}//end inner for
		}//end if not null

		//remove these objects from their appropriate ArrayLists
		//this happens after the above iterations are done
		for (Tuple tup : tupMarkForRemovals)
			tup.removeMovable();

		//add these objects to their appropriate ArrayLists
		//this happens after the above iterations are done
		for (Tuple tup : tupMarkForAdds)
			tup.addMovable();

		//call garbage collection
		System.gc();

	}//end meth

	private void killFoe(Movable movFoe) {
		//remove the original Foe
            if (movFoe instanceof Ufo) {
                CommandCenter.setUFO(false);
                CommandCenter.setScore(CommandCenter.getScore() + 500);
            }
            if (movFoe instanceof Alien && !(movFoe instanceof Ufo))
                CommandCenter.setScore(CommandCenter.getScore() + 100);
			tupMarkForRemovals.add(new Tuple(CommandCenter.movFoes, movFoe));
	}

	//some methods for timing events in the game,
	//such as the appearance of UFOs, floaters (power-ups), etc.
	public void tick() {
		if (nTick == Integer.MAX_VALUE)
			nTick = 0;
		else
			nTick++;
	}

	public static int getTick() {
		return nTick;
	}

	private void spawnNewShipFloater() {
		//make the appearance of power-up dependent upon ticks and levels
		//the higher the level the more frequent the appearance
		if (nTick % (SPAWN_NEW_SHIP_FLOATER - nLevel * 7) == 0 && CommandCenter.getNumGalagaFalcons() <= 1) {
			CommandCenter.movFloaters.add(new NewShipFloater());
		}
	}

	// Called when user presses 's'
	private void startGame() {
		CommandCenter.clearAll();
		CommandCenter.initGame();
		CommandCenter.setLevel(0);
		CommandCenter.setPlaying(true);
		CommandCenter.setPaused(false);
		if (!bMuted)
		   clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
	}

    private void spawnAliens() {
        int x = 100;
        int y = 100;
        for (int j = 0; j < getTotalRows(); j++) {
            for (int i = 0; i < Math.min((5 + CommandCenter.getLevel()),10); i++) {
                if (j % 2 == 0)
                    CommandCenter.movFoes.add(new BatAlien(x, y));
                else
                    CommandCenter.movFoes.add(new MushroomAlien(x,y));
                x+=100;
                if (i == 9)
                    setTotalRows(getTotalRows() + 1);
            }
            x = 100;
            y += 50;
        }

    }

    private void spawnBarricade(int nBarricades) {
        // you only get the 7 initial barricades, nothing after;
        System.out.println("Level is " + CommandCenter.getLevel());
        if (CommandCenter.getLevel() == 1) {
            int initX = 100;
            int initY = 550;

            for (int i = 0; i < nBarricades; i++) {
                CommandCenter.movFriends.add(new Barricade(initX,initY));
                initX += 150;
            }
        }
    }

    private void spawnUFO() {
        if (!CommandCenter.getContainsUFO() && (this.getTick() % 125 == 0) && CommandCenter.getLevel() > 1) {
            CommandCenter.movFoes.add(new Ufo());
            CommandCenter.setUFO(true);
        }
    }

    private void drawExplosion(GalagaBullet bull, double radius) {
         CommandCenter.movFloaters.add(new Explosion(bull,radius));
    }

	private boolean isLevelClear(){
		//if there are no more Aliens on the screen
        boolean bAlienFree = true;
		for (Movable movFoe : CommandCenter.movFoes) {
            if (movFoe instanceof Alien) {
                bAlienFree = false;
                break;
            }
		}
		return bAlienFree;
	}

	private void checkNewLevel(){
		if (isLevelClear()) {
            CommandCenter.setLevel(CommandCenter.getLevel() + 1);
			if (CommandCenter.getGalagaFalcon() != null)
				CommandCenter.getGalagaFalcon().setProtected(true);
            spawnAliens();
            spawnBarricade(7);
		}
	}

    private int getTotalRows() { return totalRows; }

    private void setTotalRows(int alienRows) {
        if (alienRows <= 5)
            throw new IllegalArgumentException("total rows of aliens must be > 5!");
        totalRows = alienRows;
    }

	// Varargs for stopping looping-music-clips
	private static void stopLoopingSounds(Clip... clpClips) {
		for (Clip clp : clpClips) {
			clp.stop();
		}
	}

	// ===============================================
	// KEYLISTENER METHODS
	// ===============================================

	@Override
	public void keyPressed(KeyEvent e) {
		//Falcon fal = CommandCenter.getFalcon();
        GalagaFalcon gFal = CommandCenter.getGalagaFalcon();
		int nKey = e.getKeyCode();
		if (nKey == START && !CommandCenter.isPlaying()) {
            startGame();
        }


		if (gFal != null) {
            switch (nKey) {
			case PAUSE:
				CommandCenter.setPaused(!CommandCenter.isPaused());
				if (CommandCenter.isPaused())
					stopLoopingSounds(clpMusicBackground, clpThrust);
				else
					clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
				break;
			case QUIT:
				System.exit(0);
				break;
			case UP:
				if (!CommandCenter.isPaused())
					clpThrust.loop(Clip.LOOP_CONTINUOUSLY);
				break;
			case LEFT:
                gFal.setDeltaX(-15.0);
				break;
			case RIGHT:
                gFal.setDeltaX(15.0);
				break;

			default:
				break;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		//Falcon fal = CommandCenter.getFalcon();
        GalagaFalcon gFal = CommandCenter.getGalagaFalcon();

		int nKey = e.getKeyCode();
		 //System.out.println("Key released: " + nKey);

		if (gFal != null) {
			switch (nKey) {
			case FIRE:
                CommandCenter.movFriends.add(new GalagaBullet(gFal));
				Sound.playSound("laser.wav");
				break;
			case LEFT:
                gFal.stopMoving();
				break;
			case RIGHT:
                gFal.stopMoving();
				break;
			case UP:
				clpThrust.stop();
				break;
			case MUTE:
				if (!bMuted){
					stopLoopingSounds(clpMusicBackground);
					bMuted = !bMuted;
				} 
				else {
					clpMusicBackground.loop(Clip.LOOP_CONTINUOUSLY);
					bMuted = !bMuted;
				}
				break;
			default:
				break;
			}
		}
	}

	@Override
	// Just need it b/c of KeyListener implementation
	public void keyTyped(KeyEvent e) {
	}
	

	
}

// ===============================================
// ==A tuple takes a reference to an ArrayList and a reference to a Movable
//This class is used in the collision detection method, to avoid mutating the array list while we are iterating
// it has two public methods that either remove or add the movable from the appropriate ArrayList 
// ===============================================

class Tuple{
	//this can be any one of several CopyOnWriteArrayList<Movable>
	private CopyOnWriteArrayList<Movable> movMovs;
	//this is the target movable object to remove
	private Movable movTarget;
	
	public Tuple(CopyOnWriteArrayList<Movable> movMovs, Movable movTarget) {
		this.movMovs = movMovs;
		this.movTarget = movTarget;
	}
	
	public void removeMovable(){
		movMovs.remove(movTarget);
	}
	
	public void addMovable(){
		movMovs.add(movTarget);
	}

}
