package edu.uchicago.cs.java.finalproject.game.model;

import java.awt.*;
import java.util.ArrayList;


public class AlienBullet extends Sprite {

	  private final double FIRE_POWER = 20;

    public AlienBullet(Alien al){
		
		super();

		//defined the points on a cartesean grid
		ArrayList<Point> pntCs = new ArrayList<Point>();

        pntCs.add(new Point(0,3)); //top point

        pntCs.add(new Point(1,-1));
        pntCs.add(new Point(0,-2));
        pntCs.add(new Point(-1,-1));

        assignPolarPoints(pntCs);

		//a bullet expires after 20 frames
	    setExpire(35);
        if (al instanceof Ufo) {
            setRadius(15);
            setColor(Color.CYAN);
        }
        else
	        setRadius(9);

        if (al instanceof MushroomAlien)
            setOrientation(al.getOrientation() - 180);
        else
            setOrientation(al.getOrientation());

	    //everything is relative to the alien that fired the bullet
	    setDeltaX( al.getDeltaX() +
	               Math.cos( Math.toRadians( this.getOrientation() ) ) * FIRE_POWER );
	    setDeltaY( al.getDeltaY() +
	               Math.sin( Math.toRadians( this.getOrientation() ) ) * FIRE_POWER );
	    setCenter(al.getCenter());

        setColor(al.getColor());

    }

    // custom draw for the bullet
    public void draw (Graphics g) {
        super.draw(g);
        g.setColor(this.getColor());
        g.fillPolygon(getXcoords(),getYcoords(),dDegrees.length);
    }

    //override the expire method - once an object expires, then remove it from the arrayList. 
	public void expire(){
        if (getExpire() == 0)
            CommandCenter.movFoes.remove(this);
        else if (getCenter().getY() >= getDim().height)
            CommandCenter.movFoes.remove(this);
        else if (getCenter().getX() >= getDim().width || getCenter().getX() <= 0)
            CommandCenter.movFoes.remove(this);
        else
            setExpire(getExpire() - 1);
	}

}
