package edu.uchicago.cs.java.finalproject.game.model;
import java.awt.*;
import edu.uchicago.cs.java.finalproject.controller.*;

/**
 * Created by Arnav on 11/22/14.
 * Prototype for all different alien villains
 * will be able to move, shoot, and dive; dive will be random
 */
public abstract class Alien extends Sprite {

    private boolean mRight;
    private boolean mLeft;
    private final int ALIEN_RADIUS = 21; // radius for collision detection
    private int numAliens = 0;
    private int dx; // right/left movement
    private int dy; // up/down movement
    int tick;

    public Alien() {
        dx = 10 + CommandCenter.getLevel()*2; // set X movement of alien
        dy = 0; // set Y movement of alien
        setOrientation(90);
        setRadius(ALIEN_RADIUS);
        numAliens++;
        mRight = true;
        mLeft = false;
        tick = 0;
    }

    public Alien(int xLocation, int yLocation) {
        //super();
        dx = 10 + CommandCenter.getLevel()*2; // set X movement of alien
        dy = 0; // set Y movement of alien
        //setDeltaX(dx);
        //setDeltaY(dy);
        setOrientation(90);
        setRadius(ALIEN_RADIUS);
        // location is not predetermined; determined by location of sprites around it
        setCenter(new Point(xLocation, yLocation));
        numAliens++;
        mRight = true;
        mLeft = false;
        tick = 0;

    }

    public int getDx() {
        return dx;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public int getDy() {
        return dy;
    }

    public void setDy(int dy) {
        this.dy = dy;
    }

    public void tick() {
        if (tick == Integer.MAX_VALUE)
            tick = 0;
        else
            tick++;
    }

    public int getTick() {
        return tick;
    }

    // alien will move left and right only, unless it hits an edge, in which case it will move down
    //@Override
    public void move() {
        Point pnt = getCenter();

        if (pnt.x > getDim().width) {
            pnt.y += 20;
            mRight = false;
            mLeft = true;
        }
        else if (pnt.x < 0) {
            pnt.y += 20;
            mRight = true;
            mLeft = false;
        }

        if (mRight)
            setCenter(new Point(pnt.x + dx, pnt.y));
        if (mLeft)
            setCenter(new Point(pnt.x - dx, pnt.y));

        if (willShoot() && ((Game.getTick() % 2) == 0))
            shoot();
    }

    public int getNumAliens() {
        return numAliens;
    }

    // on kill, decrement number of aliens available
    public void killAlien() {
        numAliens--;
    }

    // determines if alien will shoot it's gun
    public boolean willShoot() {
        return Math.random() > (0.8 - (CommandCenter.getLevel()/10));
    }

    public boolean getRight() {
        return mRight;
    }

    public boolean getLeft() {
        return mLeft;
    }

    public void setRight(boolean bool) {
        mRight = bool;
        mLeft = !mRight;
    }

    public void setLeft(boolean bool) {
        mLeft = bool;
        mRight = !mLeft;
    }

    // alien shoots
    public void shoot() {
        if (willShoot())
            CommandCenter.movFoes.add(new AlienBullet(this));
    }


}
