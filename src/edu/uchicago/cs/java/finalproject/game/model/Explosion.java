package edu.uchicago.cs.java.finalproject.game.model;

import java.awt.*;
/**
 * Created by Arnav on 12/1/14.
 */
public class Explosion extends Sprite {

    private double radius;

    public Explosion(GalagaBullet bull, double radius) {
        super();
        setCenter(bull.getCenter());

        if (radius <= 0)
            throw new IllegalArgumentException("must have positive radius for circle!");

        this.radius = radius;
        setExpire(4);
        setColor(bull.getColor());
    }

    public double getExplosionRadius() {
        return radius;
    }

    public void setExplosionRadius(double radius) {
        if (radius <= 0)
            throw new IllegalArgumentException("must have positive radius for circle!");
        this.radius = radius;
    }

    public void draw(Graphics g) {
        g.setColor(this.getColor());
        Point pnt = this.getCenter();
        g.fillOval(pnt.x,pnt.y,(int) radius, (int) radius);
    }

    public void expire() {
        if (getExpire() == 0)
            CommandCenter.movFloaters.remove(this);
        else {
            if (getExpire() % 4 == 0)
                setColor(Color.GREEN);
            else if (getExpire() % 4 == 1)
                setColor(Color.BLUE);
            else if (getExpire() % 4 == 2)
                setColor(Color.RED);
            else
                setColor(Color.MAGENTA);
            setRadius(this.getRadius() + 5);
            setExpire(getExpire() - 1);
        }

    }
}
