package edu.uchicago.cs.java.finalproject.game.model;

import java.util.concurrent.CopyOnWriteArrayList;

import edu.uchicago.cs.java.finalproject.sounds.Sound;

// I only want one Command Center and therefore this is a perfect candidate for static
// Able to get access to methods and my movMovables ArrayList from the static context.
public class CommandCenter {

	//private static int nNumFalcon;
    private static int nNumGalagaFalcon;
	private static int nLevel;
	private static long lScore;
    private static GalagaFalcon gFalShip;
	private static boolean bPlaying;
	private static boolean bPaused;
    private static boolean containsUFO;
	
	// These ArrayLists are thread-safe
	public static CopyOnWriteArrayList<Movable> movDebris = new CopyOnWriteArrayList<Movable>();
	public static CopyOnWriteArrayList<Movable> movFriends = new CopyOnWriteArrayList<Movable>();
	public static CopyOnWriteArrayList<Movable> movFoes = new CopyOnWriteArrayList<Movable>();
	public static CopyOnWriteArrayList<Movable> movFloaters = new CopyOnWriteArrayList<Movable>();

	// Constructor made private - static Utility class only
	private CommandCenter() {}
	
	public static void initGame(){
		setLevel(1);
		setScore(0);
        setNumGalagaFalcons(3);
		spawnGalagaFalcon(true);
        setUFO(false);
	}

    public static void spawnGalagaFalcon(boolean bFirst) {
        if (getNumGalagaFalcons() != 0) {
            gFalShip = new GalagaFalcon();
            movFriends.add(gFalShip);
            if (!bFirst)
                setNumGalagaFalcons(getNumGalagaFalcons() - 1);
            Sound.playSound("shipspawn.wav");
        }
    }
	
	public static void clearAll(){
		movDebris.clear();
		movFriends.clear();
		movFoes.clear();
		movFloaters.clear();
	}

	public static boolean isPlaying() {
		return bPlaying;
	}

	public static void setPlaying(boolean bPlaying) {
		CommandCenter.bPlaying = bPlaying;
	}

	public static boolean isPaused() {
		return bPaused;
	}

	public static void setPaused(boolean bPaused) {
		CommandCenter.bPaused = bPaused;
	}
	
	public static boolean isGameOver() {		//if the number of falcons is zero, then game over
		return getNumGalagaFalcons() == 0;
	}

    public static void setUFO(boolean bool) {
        containsUFO = bool;
    }

    public static boolean getContainsUFO() {
        return containsUFO;
    }

	public static int getLevel() {
		return nLevel;
	}

	public  static long getScore() {
		return lScore;
	}

	public static void setScore(long lParam) {
		lScore = lParam;
	}

	public static void setLevel(int n) {
		nLevel = n;
	}

    public static GalagaFalcon getGalagaFalcon() { return gFalShip; }

	public static CopyOnWriteArrayList<Movable> getMovDebris() {
		return movDebris;
	}

	public static CopyOnWriteArrayList<Movable> getMovFriends() {
		return movFriends;
	}

	public static CopyOnWriteArrayList<Movable> getMovFoes() {
		return movFoes;
	}

	public static CopyOnWriteArrayList<Movable> getMovFloaters() {
		return movFloaters;
	}

    public static int getNumGalagaFalcons() {
        return nNumGalagaFalcon;
    }

    public static void setNumGalagaFalcons(int n) {
        nNumGalagaFalcon = n;
    }
}
