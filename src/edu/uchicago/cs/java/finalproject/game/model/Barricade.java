package edu.uchicago.cs.java.finalproject.game.model;
import java.awt.*;
import java.util.*;

/**
 * Created by Arnav on 11/24/14.
 * Placed above the space ship; can take damage without dying; ship can hide behind barricade to not get shot
 */
public class Barricade extends Sprite {

    private int health;
    private static final int HEALTH_INCREMENT = 10;

    public Barricade(int xLocation, int yLocation) {
        setCenter(new Point(xLocation, yLocation));
        health = 100;

        ArrayList<Point> pts = new ArrayList<Point>();

        pts.add(new Point(-3,-3));
        pts.add(new Point(-3,1));
        pts.add(new Point(-1,3));
        pts.add(new Point(3,3));
        pts.add(new Point(5,1));
        pts.add(new Point(5,-3));

        setRadius(45);
        setOrientation(270); // point 'upward'
        setColor(Color.white);

        assignPolarPoints(pts);
    }

    public int getHealth() {
        return health;
    }

    public void move() {

    }

    public void draw(Graphics g) {
        super.draw(g);
        g.setColor(Color.white);
        g.fillPolygon(getXcoords(),getYcoords(),dDegrees.length);
    }

    public void expire() {
        if (health == 0)
            CommandCenter.movFriends.remove(this);
    }

    public void decreaseHealth() {
        health -= HEALTH_INCREMENT;
    }
}
