package edu.uchicago.cs.java.finalproject.game.model;

import java.util.*;
import java.awt.*;

/**
 * Created by Arnav on 11/30/14.
 */
public class MushroomAlien extends Alien {

    private static final int MUSH_ALIEN_RAD = 35;

    public MushroomAlien(int xLocation, int yLocation) {
        super(xLocation, yLocation);

        ArrayList<Point> pts = new ArrayList<Point>();

        pts.add(new Point(-3,-3));
        pts.add(new Point(-3,-2));
        pts.add(new Point(-2,-2));
        pts.add(new Point(-2,-1));
        pts.add(new Point(-4,-1));
        pts.add(new Point(-4,2));
        pts.add(new Point(-3,2));
        pts.add(new Point(-3,3));
        pts.add(new Point(-1,3));
        pts.add(new Point(-1,4));
        pts.add(new Point(3,4));
        pts.add(new Point(3,3));
        pts.add(new Point(5,3));
        pts.add(new Point(5,2));
        pts.add(new Point(6,2));
        pts.add(new Point(6,-1));
        pts.add(new Point(4,-1));
        pts.add(new Point(4,-2));
        pts.add(new Point(5,-2));
        pts.add(new Point(5,-3));
        pts.add(new Point(3,-3));
        pts.add(new Point(3,-2));
        pts.add(new Point(2,-2));
        pts.add(new Point(2,-3));
        pts.add(new Point(0,-3));
        pts.add(new Point(0,-2));
        pts.add(new Point(-1,-2));
        pts.add(new Point(-1,-3));

        assignPolarPoints(pts);

        setRadius(MUSH_ALIEN_RAD);
        setColor(Color.MAGENTA);
        setOrientation(270);
        if (CommandCenter.getLevel() % 2 != 0)
            setLeft(true);
    }

    public void draw(Graphics g) {
        super.draw(g);
        g.setColor(Color.MAGENTA);
        g.drawPolygon(getXcoords(),getYcoords(),dDegrees.length);
    }




}
