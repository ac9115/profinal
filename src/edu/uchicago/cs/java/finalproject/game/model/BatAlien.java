package edu.uchicago.cs.java.finalproject.game.model;
import java.awt.*;
import java.util.*;

/**
 * Created by Arnav on 11/22/14.
 */
public class BatAlien extends Alien {

    private final int BATALIEN_RAD = 21;

    public BatAlien(int xLocation, int yLocation) {
        super(xLocation, yLocation);

        ArrayList<Point> pts = new ArrayList<Point>();

        pts.add(new Point(-7, 4));
        pts.add(new Point(-4,1));
        pts.add(new Point(-2,4));
        pts.add(new Point(2,4));
        pts.add(new Point(4,1));
        pts.add(new Point(7,4));
        pts.add(new Point(7,-4));
        pts.add(new Point(4,-2));
        pts.add(new Point(2,-4));
        pts.add(new Point(-2,-4));
        pts.add(new Point(-4,-2));
        pts.add(new Point(-7,-4));

        pts = doubleSpriteSize(pts);

        assignPolarPoints(pts);

        setColor(Color.red);
        setRadius(BATALIEN_RAD);

    }

    @Override
    public void draw(Graphics g) {
        super.draw(g);
        g.setColor(Color.red);
        g.drawPolygon(getXcoords(),getYcoords(),dDegrees.length);
    }

    private ArrayList<Point> doubleSpriteSize(ArrayList<Point> pts) {
        for (Point p : pts) {
            p.setLocation(p.getX()*3,p.getY()*3);
        }
        return pts;
    }
}
