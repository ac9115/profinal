package edu.uchicago.cs.java.finalproject.game.model;

import edu.uchicago.cs.java.finalproject.controller.Game;

import java.awt.*;
import java.util.*;
/**
 * Created by Arnav on 12/2/14.
 */
public class Ufo extends Alien {

    private static final int UFO_RADIUS = 35;
    private static final int STARTING_X = 100;
    private static final int STARTING_Y = 75;

    public Ufo() {
        super();
        ArrayList<Point> pts = new ArrayList<Point>();
        createFigure(pts);
        assignPolarPoints(pts);
        setCenter(new Point(STARTING_X,STARTING_Y));
        setRadius(UFO_RADIUS);
        setDx(CommandCenter.getLevel()*2 + 20);
        setDy(0);
        setColor(Color.red);
    }

    public void move() {
        //if ((Math.random() > 0.6) && (Game.getTick() % 300 == 0));
            //setLeft(true);

        Point pnt = getCenter();
        tick();

        if (pnt.x > getDim().width) {
            setRight(false);
        }
        else if (pnt.x < 0) {
            setRight(true);
        }

        if (getRight())
            setCenter(new Point(pnt.x + getDx(), pnt.y));
        if (getLeft())
            setCenter(new Point(pnt.x - getDx(), pnt.y));

        shoot();
    }

    public void draw(Graphics g) {
        super.draw(g);
        setColor(this.getColor());
        g.fillPolygon(getXcoords(),getYcoords(),dDegrees.length);
    }

    private void createFigure(ArrayList<Point> pts) {
        pts.add(new Point(0,-3));
        pts.add(new Point(-1,-3));
        pts.add(new Point(-1,-2));
        pts.add(new Point(-3,-2));
        pts.add(new Point(-3,-3));
        pts.add(new Point(-4,-3));
        pts.add(new Point(-4,-4));
        pts.add(new Point(-5,-4));
        pts.add(new Point(-5,-3));
        pts.add(new Point(-6,-3));
        pts.add(new Point(-6,-2));
        pts.add(new Point(-7,-2));
        pts.add(new Point(-7,-1));
        pts.add(new Point(-6,-1));
        pts.add(new Point(-6,0));
        pts.add(new Point(-5,0));
        pts.add(new Point(-5,1));
        pts.add(new Point(-4,1));
        pts.add(new Point(-4,2));
        pts.add(new Point(-2,2));
        pts.add(new Point(-2,3));
        pts.add(new Point(0,3));

        ArrayList<Point> symmetric = new ArrayList<Point>();
        for (Point p : pts)
            symmetric.add(new Point(p.x*-1,p.y));

        for (int i = symmetric.size() - 1; i >= 0; i--)
            pts.add(symmetric.get(i));
    }


}
